package model

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model      // Adds some metadata fields to the table
	Id         uint `gorm:"primaryKey;autoIncrement"`
	Username   string
	Email      string
	Password   string
}

func (*User) tableName() string {
	return "public.users"
}
