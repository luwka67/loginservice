package model

import (
	"github.com/golang-jwt/jwt"
	"gorm.io/gorm"
)

type Token struct {
	gorm.Model
	Id     int
	UserID int    `json:"user_id"`
	Email  string `json:"email"`
	jwt.StandardClaims
}
