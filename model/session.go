package model

import "time"

type Sesion struct {
	Id        uint      `gorm:"primaryKey;autoIncrement"`
	Token     string    `json:"token" gorm:"index"`
	CreatedAt time.Time `json:"-"`
	Expires   time.Time `json:"-"`
	Username  string
}
