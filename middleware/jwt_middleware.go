package middleware

import (
	"fmt"
	"login_service/database"
	"login_service/model"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt"
)

var jwtSecret = []byte("your-secret-key") // Replace with your secret key

// GenerateToken generates a JWT token for the given claims
func GenerateToken(userID int, email string) (string, error) {
	claims := model.Token{
		UserID: userID,
		Email:  email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * 30).Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString(jwtSecret)

}

// Define a middleware for JWT authentication
func JWTMiddleware(c *fiber.Ctx) error {
	db := database.DB
	users_tables_db := db.Table("users")
	// Extract the JWT token from the request header
	authHeader := c.Get("Authorization")
	if authHeader == "" {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "Unauthorized",
			"status": 401})
	}

	// Check if the Authorization header has the "Bearer " prefix
	const bearerPrefix = "Bearer "
	if strings.HasPrefix(authHeader, bearerPrefix) {
		// Remove the "Bearer " prefix
		token := strings.TrimPrefix(authHeader, bearerPrefix)

		// Verify the token
		claims := &model.Token{}
		jwtToken, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
			return jwtSecret, nil
		})

		if err != nil || !jwtToken.Valid {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "Unauthorized"})
		}

		// Check token expiration
		if claims.ExpiresAt < time.Now().Unix() {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "Token has expired"})
		}
		var user model.User
		fmt.Println("email =", claims.Email)
		if err := users_tables_db.Where("email = ?", claims.Email).First(&user).Error; err != nil {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"error": "User not found"})
		}
		// Set the user information in the context for later use
		c.Locals("user", claims.Email)
		return c.Next()
	}

	return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "Invalid Authorization header format"})
}
