package router

import (
	userController "login_service/controller/user"
	userRoutes "login_service/router/user"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

func SetupRoutes(app *fiber.App) {
	api := app.Group("/apis", logger.New())

	api.Post("/signUp", userController.CreateUsers)

	api.Post("/login", userController.LoginUser)

	userRoutes.SetupNoteRoutes(api)
}
