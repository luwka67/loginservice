package userRoutes

import (
	userController "login_service/controller/user"
	"login_service/middleware"

	"github.com/gofiber/fiber/v2"
)

func SetupNoteRoutes(router fiber.Router) {
	// user := router.Group("/user")
	// user.Get("/dashboard", middleware.JWTMiddleware, userController.DashboardHandler)
	// // Create a user
	// //user.Post("/", middleware.JWTMiddleware, userController.CreateUsers)
	// // Read all users
	// user.Get("/all", middleware.JWTMiddleware, userController.GetUsers)
	// // // Read one user
	// user.Get("/:userId", middleware.JWTMiddleware, userController.GetUser)
	// // // Update one user
	// user.Put("/:userId", middleware.JWTMiddleware, userController.UpdateUser)
	// // // Delete one user
	// user.Delete("/:userId", middleware.JWTMiddleware, userController.DeleteUser)
	dahsboard := router.Group("/dashboard")

	dahsboard.Get("/Detail", middleware.JWTMiddleware, userController.UserDetail)
	dahsboard.Get("/Home", middleware.JWTMiddleware, userController.UserHome)
	dahsboard.Get("/Profile", middleware.JWTMiddleware, userController.UserProfile)
	dahsboard.Post("/ChangePassword", middleware.JWTMiddleware, userController.UpdateUserPassword)
}
