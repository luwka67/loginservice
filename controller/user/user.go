package userController

import (
	"fmt"
	"login_service/database"
	"login_service/middleware"
	"login_service/model"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt"
	"golang.org/x/crypto/bcrypt"
)

func GetUsers(c *fiber.Ctx) error {
	db := database.DB
	userTableDb := db.Table("users")
	var users []model.User

	userTableDb.Find(&users)

	if len(users) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No notes present", "data": nil})
	}

	return c.JSON(fiber.Map{"status": "success", "message": "Notes Found", "data": users})
}
func DashboardHandler(c *fiber.Ctx) error {
	db := database.DB
	user_table := db.Model(&model.User{})
	// Extract user information from the context
	user, ok := c.Locals("users").(*jwt.Token)
	if !ok {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": "User information not available"})
	}
	claims := user.Claims.(jwt.MapClaims)
	email := claims["email"].(string)

	var current_user model.User

	if err := user_table.Where("email = ?", email).First(&current_user).Error; err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": "Бүртгэгдээгүй хэрэглэгч байна"})
	}
	return c.JSON(fiber.Map{"id": current_user.Id,
		"email":    current_user.Email,
		"username": current_user.Username})
}
func logoutHandler(c *fiber.Ctx) error {
	// Extract user information from the context
	db := database.DB
	userTableDb := db.Table("users")
	user, ok := c.Locals("users").(*jwt.Token)
	if !ok {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": "User information not available"})
	}

	claims := user.Claims.(jwt.MapClaims)
	email := claims["email"].(string)

	// Find the user in the database
	var existingUser model.User
	if err := userTableDb.Where("email = ?", email).First(&existingUser).Error; err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"error": "User not found"})
	}

	// Remove the token value from the user and database
	userID := existingUser.ID
	var session model.Sesion
	sesionsTableDb := db.Table("sesions")
	result := sesionsTableDb.Where("userID = ?", userID).First(session)
	if result != nil {
		panic(result)
	}
	session.Token = ""
	sesionsTableDb.Save(&session)

	return c.JSON(fiber.Map{"message": "Logout successful"})
}
func UserDetail(c *fiber.Ctx) error {
	return c.JSON(fiber.Map{"message": "user detail"})
}
func UserHome(c *fiber.Ctx) error {
	return c.JSON(fiber.Map{"message": "user home"})
}

type NavBar struct {
	Id    uint
	Name  string
	Route string
	Icon  string
}

func (*NavBar) TableName() string {
	return "nav_bar"
}

func GetNavList() []NavBar {
	list := make([]NavBar, 0)

	// db := database.DB.Model(&NavBar{})

	// if err := db.Find(&list).Error; err != nil {
	// 	return err
	// }

	return list
}
func UserProfile(c *fiber.Ctx) error {
	db := database.DB
	user_table := db.Model(&model.User{})
	// Extract user information from the context
	fmt.Println("log -----------------------------------------")
	email, ok := c.Locals("user").(string)
	fmt.Println("log -----------------------------------------")
	if !ok {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": ok})
	}
	fmt.Println("log2 -----------------------------------------", email)
	var current_user model.User

	if err := user_table.Where("email = ?", email).First(&current_user).Error; err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": "Бүртгэгдээгүй хэрэглэгч байна"})
	}
	return c.JSON(fiber.Map{"id": current_user.Id,
		"email":    current_user.Email,
		"username": current_user.Username})
}
func CreateUsers(c *fiber.Ctx) error {
	db := database.DB
	userTableDb := db.Model(&model.User{})
	user := new(model.User)

	err := c.BodyParser(user)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}

	if err := userTableDb.Where("username = ?", user.Username).First(user).Error; err == nil {
		return c.Status(404).JSON(fiber.Map{"message": "Username is already taken"})
	}
	var userByEmail model.User
	result := db.Where("email = ?", user.Email).First(&userByEmail)
	if result.RowsAffected > 0 {
		return c.Status(404).JSON(fiber.Map{"message": "Email is already taken"})
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"message": "Error hashing password"})
	}

	user.Password = string(hashedPassword)

	err = db.Create(&user).Error
	if err != nil {
		return c.Status(501).JSON(fiber.Map{"status": "error", "message": "Could not create user", "data": err})
	}

	return c.JSON(fiber.Map{"status": "success", "message": "Created user", "data": user})
}
func GetUser(c *fiber.Ctx) error {
	db := database.DB
	userTableDb := db.Table("users")
	var user model.User

	// Read the param noteId
	id := c.Params("userId")

	// Find the note with the given Id
	userTableDb.Find(&user, "id = ?", id)

	// Return the note with the Id
	return c.JSON(fiber.Map{"status": "success", "message": "Notes Found", "data": user})
}
func UpdateUser(c *fiber.Ctx) error {
	type updateUser struct {
		Email    string `json:"email"`
		Password string `json:"passwword"`
		Username string `json:"username"`
	}
	db := database.DB
	var user model.User

	// Read the param noteId
	id := c.Params("userId")

	// Find the note with the given Id
	result := db.Find(&user, "id = ?", id)

	// Check if the user exists
	if result.RowsAffected == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "User not found", "data": nil})
	}

	// Store the body containing the updated data and return error if encountered
	var updateNoteData updateUser
	err := c.BodyParser(&updateNoteData)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}

	// Edit the note
	user.Email = updateNoteData.Email
	user.Password = updateNoteData.Password
	user.Username = updateNoteData.Username

	// Save the Changes
	db.Save(&user)

	// Return the updated note
	return c.JSON(fiber.Map{"status": "success", "message": "Notes Found", "data": user})
}
func UpdateUserPassword(c *fiber.Ctx) error {

	email, ok := c.Locals("user").(string)
	if !ok {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": ok})
	}
	type UpdateUser struct {
		CurrentPassword string `json:"current_password"`
		NewPassword     string `json:"new_password"`
		ReNewPassword   string `json:"re_new_password"`
	}
	db := database.DB
	userTableDb := db.Table("users")

	user := new(model.User)

	// Store the body containing the updated data and return error if encountered
	updateUserData := new(UpdateUser)
	err := c.BodyParser(&updateUserData)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}

	fmt.Println("==================", user, email)

	if err := userTableDb.Where("email = ?", email).First(&user).Error; err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": "Бүртгэгдээгүй хэрэглэгч байна"})
	}

	err1 := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(updateUserData.CurrentPassword))
	if err1 != nil {
		return c.Status(404).JSON(fiber.Map{"message": "нууц үг ээ зөв оруулна уу"})
	}
	if updateUserData.NewPassword != updateUserData.ReNewPassword {
		return c.Status(404).JSON(fiber.Map{"message": "нууц үг ээ зөв оруулна уу"})
	}
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(updateUserData.NewPassword), bcrypt.DefaultCost)
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"message": "Error hashing password"})
	}

	user.Password = string(hashedPassword)
	// Edit the note

	// Save the Changes
	db.Save(&user)

	// Return the updated note
	return c.JSON(fiber.Map{"status": "success", "message": "User Found", "data": user})
}
func DeleteUser(c *fiber.Ctx) error {
	db := database.DB
	var user model.User

	id := c.Params("userId")

	// Find the note with the given Id
	result := db.Find(&user, "id = ?", id)

	// Check if the user exists
	if result.RowsAffected == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "User not found", "data": nil})
	}

	// Delete the note and return error if encountered
	err := db.Delete(&user, "id = ?", id).Error

	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Failed to delete note", "data": nil})
	}

	// Return success message
	return c.JSON(fiber.Map{"status": "success", "message": "Deleted Note"})
}

func LoginUser(c *fiber.Ctx) error {
	// Your login logic here...
	var loginRequest struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}
	db := database.DB

	if err := c.BodyParser(&loginRequest); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Invalid request payload"})
	}

	var user model.User
	if err := db.Where("email = ?", loginRequest.Email).First(&user).Error; err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "Invalid credentials"})
	}

	// Compare passwords
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(loginRequest.Password))
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "Invalid credentials"})
	}

	// Generate a JWT token
	token, err := middleware.GenerateToken(int(user.ID), user.Email)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"message": "Error generating token"})
	}
	sesion := createSession(token, user.Username)
	db.Create(&sesion)

	return c.JSON(fiber.Map{"message": "Login successful", "token": token})
}
func createSession(token string, username string) model.Sesion {
	var session model.Sesion
	session.Token = token
	session.Username = username
	fmt.Println("-----------------------", token)
	session.CreatedAt = time.Now()
	session.Expires = time.Now().Add(time.Minute)
	return session
}
